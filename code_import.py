import code_extract_specific_problem as code_extract
import sys
import getopt
import re
def removeNonAscii(s):
	return "".join(i for i in s if ord(i)<128)
def stripcomments(text):
	return re.sub('//.*?(\r\n?|\n)|/\*.*?\*/', '', text, flags=re.S)
def remove_lib(text):
	return re.sub(r'^#include.*\n?', '', text, flags=re.MULTILINE)


def codeimport(contest_id,problem_char,page_number,k):
	submission_id = []
#def stripcomments(text):
#	return re.sub('//.*?(\r\n?|\n)|/\*.*?\*/', '', text, flags=re.S)


	for i in range(1,int(page_number)):
		submission_id = submission_id + code_extract.get_submission_id(contest_id,problem_char,page_number)
	submission_id=[el.replace('\n', '') for el in submission_id]
	list_code=[]
 
	with open('train_binary','a') as f:
		for i in submission_id:
			source=code_extract.extract_source_code(contest_id,i)
			list_code+=[source]
			if source:
				source=(source).replace('\r','')
				source=stripcomments(source)
				source=remove_lib(source)
				source=(source).replace('\n','')
				source=(source).replace('$','')
				source=removeNonAscii(source)
				f.write(str(k)+"$"+source.encode('utf-8')+"\n")
				k+=1
	return k
