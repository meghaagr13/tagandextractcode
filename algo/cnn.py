#script to runn cnn code
import numpy as np
np.random.seed(123)

import  pandas as pd

from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Convolution1D, MaxPooling1D
from keras.utils import np_utils
from keras.optimizers import RMSprop
from keras.preprocessing import sequence

if __name__ == '__main__':
	
	#taking test and train data in pandas dataframe
	train = pd.read_csv('q', header=0,delimiter = "$",quoting = 2)
	test = pd.read_csv('Sample',header = 0,delimiter= "$", quoting = 2)
	
	#converting x_train, x_test from pandas dataframe to numpy arrays
	x_train=[]
	x_test=[]
	for codes in train["code"]:
		z = []
		for s in codes:
			if ord(s)<=128 and ord(s)>=32:
				z+=[ord(s)-32]
			else:
				z+=[0]
		#making list of lists
		x_train.append(z)
	#making array of list of lists
	x_train=np.array(x_train)
	#print x_train
	
	for codes in test["code"]:
		z =[]
		for s in codes:
			if ord(s)<=128 and ord(s)>=32:
				z+=[ord(s)-32]
			else:
				z+=[0]
		x_test.append(z)
	x_test=np.array(x_test)
	#print x_test
	

	#converting y_train from pandas dataframe to numpy arrays
	y_train = train["tag"].values
	y_test = test["tag"].values	

	print x_train.shape
	
	MAXLEN = 1600
	
	x_train = sequence.pad_sequences(x_train, maxlen=MAXLEN)
	x_test = sequence.pad_sequences(x_test, maxlen=MAXLEN)
	x_train=x_train.reshape(x_train.shape[0],MAXLEN,1)
	x_test=x_test.reshape(x_test.shape[0],MAXLEN,1)
	y_test =np_utils.to_categorical(y_test,2)	
	y_train =np_utils.to_categorical(y_train,2)	

	model = Sequential() #add layers in steps as described below
	model.add(Convolution1D(32,50,strides=1,padding = 'same', activation = 'relu',input_shape=(MAXLEN,1)))#width height try reversing 
	# first layer is convulation layer with 16 output channels and fed with input vector of size 800 and zero padding
	#another convulation layer with 32 filters same size as above due to zero padding 
	model.add(MaxPooling1D(pool_size=4))
	model.add(Convolution1D(64,25,strides=1,padding='same', activation ='relu')) 
	#adding pooling layer with 4*1 size
	model.add(MaxPooling1D(pool_size=4))
	#another pool layer vector size=50
	model.add(Flatten())
	model.add(Dense(256,activation='relu'))
	model.add(Dropout(0.5))
	model.add(Dense(2, activation='softmax'))
	#complete connection with 256 neurons in softmax
	model.compile(loss='binary_crossentropy',optimizer='adam',metrics=['accuracy'])
	print model.summary()

#	model.fit(x_train,y_train,nb_epoch=10 ,verbose=1 )	
#	scores=model.evaluate(x_test,y_test,verbose=0)
		
#	model.fit(x_train,y_train,epochs=1 ,verbose=1 )	
#	scores,acc=model.evaluate(x_test,y_test,verbose=1)
	model.fit(x_train,y_train,epochs=3,shuffle=True )	
	scores =model.predict(x_test,verbose=1)
	print scores
	#model.test_on_batch(x_train,y_train)
	scores,acc=model.evaluate(x_test,y_test,verbose=1)

	print scores
	print acc	

