
import os
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.ensemble import RandomForestClassifier
from KaggleWord2VecUtility import KaggleWord2VecUtility
import pandas as pd
import numpy as np
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.ensemble import RandomForestClassifier
import stopword 
if __name__ == '__main__':
	train = pd.read_csv( "../b", header=0,delimiter='$' , quoting=2 )
	print train
	clean_train_code=[]
	for i in xrange(0,len(train["code"])):
		clean_train_code.append(" ".join(KaggleWord2VecUtility.review_to_wordlist(train["code"][i], True)))
		
#	print clean_train_code
	vectorizer = CountVectorizer(analyzer="word",tokenizer=None,preprocessor=None,stop_words=None,max_features=100)
	train_data_features = vectorizer.fit_transform(clean_train_code)

	vocab=vectorizer.get_feature_names()
	print vocab
	forest=RandomForestClassifier(n_estimators = 5)
	forest=forest.fit(train_data_features,train["tag"])
	test=pd.read_csv("../test", header=0, delimiter="$",quoting=2)
	#print test.shape	
	num_code=len(test["code"])
	clean_test_code=[]
	#for i in xrange(0,num_code):
	#	clean_code=review_to_words(test["code"][i])
	#	clean_test_code.append(clean_code)
	for i in xrange(0,len(test["code"])):
		clean_test_code.append(" ".join(KaggleWord2VecUtility.review_to_wordlist(test["code"][i], True)))

	test_data_feature=vectorizer.transform(clean_test_code)
	test_data_feature=test_data_feature.toarray()
	result=forest.predict(test_data_feature)
	print result
	output=pd.DataFrame(data={"id":test["id"],"binary":result})
	output.to_csv("Bag.csv",index=False,quoting=3)
